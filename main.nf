// the output directory and the files to process are defined in a config file:
config_file=file(params.file)
outdir=params.outdir

process process_1 {
	publishDir outdir, mode: 'copy', overwrite: true
	stageInMode 'copy'
	stageOutMode 'copy'
	
    input:
    file config from config_file

    output:
	stdout into out1
    file('*') into outfiles1

    """
	# replace output dir in config file with Nextflow work dir
	wd=\$(pwd)
	echo \$wd > config_mod
	tail -n +2 $config >> config_mod
	# call the program with modified config file
	python ${baseDir}/bin/dummy_pipeline.py setup config_mod
    """
}

process process_2 {
	publishDir outdir, mode: 'copy', overwrite: true
	stageInMode 'copy'
	stageOutMode 'copy'
	
    input:
    file config from config_file
	file('*') from outfiles1

    output:
	stdout into out2
    file('*') into outfiles2

    """
	# replace output dir in config file with Nextflow work dir
	wd=\$(pwd)
	echo \$wd > config_mod
	tail -n +2 $config >> config_mod
	# call the program with modified config file
	python ${baseDir}/bin/dummy_pipeline.py process config_mod
    """
}

out1.subscribe{ print "stdout: ${it}--\n" }
out2.subscribe{ print "stdout: ${it}--\n" }

