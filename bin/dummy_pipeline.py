import sys
import os.path

todo = sys.argv[1]
configfile = sys.argv[2]

print("Reading config")
# the output directory and the files 1 & 2 to process are defined in a file
with open(configfile, 'r') as fp1:
    outdir = fp1.readline()[:-1]
    file1 = fp1.readline()[:-1]
    file2 = fp1.readline()[:-1]


if ( todo == "setup" ):
    # Create output dir and output text file
    # write contents of files 1 & 2 to outfile
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    outfile1 = os.path.join(outdir, 'p1.txt')
    with open(outfile1, 'a') as fp2:
        with open(file1, 'r') as fp3:
            alllines = fp3.readlines()
            fp2.writelines(alllines)
        with open(file2, 'r') as fp4:
            alllines = fp4.readlines()
            fp2.writelines(alllines)

    print("Setup done")
    
elif ( todo == "process" ):
    # read again from file 1 and add another output file
    outfile2 = os.path.join(outdir, 'p2.txt')
    with open(outfile2, 'w') as fp2:
        fp2.write("We are processing\n")
        with open(file1, 'r') as fp3:
            alllines = fp3.readlines()
            fp2.writelines(alllines)
    print("Process 1 done")
        
